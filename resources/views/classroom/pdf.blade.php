<!DOCTYPE html>
<html>
<head>
	<title>Classroom List</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/bootstrap/css/bootstrap.min.css') }}">
</head>
<body>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 align="center">Classroom List</h3>
		</div>

		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Classname</th>
						<th>Teacher Name</th>
						<th>Student</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($classroom as $data)
					<tr>
						<td>{{ ++$number }}</td>
						<td>{{ $data->classname }}</td>
						<td>{{ $data->teachername }}</td>
						<td>{{ $data->studentname }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>