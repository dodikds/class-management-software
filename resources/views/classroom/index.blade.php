@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>List of Classroom
						<a href="{{ route('classroom.create') }}" class="btn btn-success pull-right">Insert</a><br></h4>
				</div>

				<div class="panel-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>ID</th>
								<th>Class name</th>
								<!-- <th>Teacher</th> -->
								<!-- <th>Student</th> -->
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
							@foreach ($classroom as $class)
							<tr>
								<td>{{ $class->class_id }}</td>
								<td>{{ $class->name }}</td>
								<!-- <td>
									@foreach($teacher as $guru)
									    {{ $guru->teacher_id == $class->teacher_id ? $guru->name : '' }}
									@endforeach
								</td> -->
								<!-- <td>{{ $class->student_id }}</td> -->
								<td>
									<form method="POST" action="{{ route('classroom.destroy', $class->class_id) }}">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<a href="{{ route('classroom.edit', $class->class_id) }}" class="btn btn-primary">Edit</a>
										<button type="submit" class="btn btn-danger">Delete</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection