@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Edit Classroom<br></h4>
				</div>

				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('classroom.update', $classroom->class_id) }}">
						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						@if ($errors->any())
						    <div class="alert alert-danger"><ul>
						    	@foreach ($errors->all() as $error)
						    	    <li>{{ $error }}</li>
						    	@endforeach
						    </ul></div>
						@endif

						<div class="form-group">
							<label for="name" class="col-md-3 control-label">Name of Classroom</label>
							<div class="col-md-6">
								<input id="name" type="text" class="form-control" name="name" value="{{ $classroom->name }}" autofocus>
							</div>
						</div>

						<!-- <div class="form-group">
							<label for="teacher" class="col-md-3 control-label">Teacher</label>
							<div class="col-md-6">
								<select id="teacher" class="form-control" name="teacher">
									<option value=""> -- Choose Teacher -- </option>
									@foreach($teacher as $teacher)
									    <option value="{{ $teacher->teacher_id }}" {{ $classroom->teacher_id == $teacher->teacher_id ? 'selected' : '' }}>{{ $teacher->name }}</option>
									@endforeach
								</select>
							</div>
						</div> -->

						<div class="form-group">
							<div class="col-md-3 col-md-offset-3">
								<button type="submit" class="btn btn-primary">Save</button>
								<a href="{{ url('classroom') }}" class="btn btn-warning">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection