@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Edit Teacher<br></h4>
				</div>

				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('teacher.update', $teacher->teacher_id) }}">
						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						@if ($errors->any())
						    <div class="alert alert-danger"><ul>
						    	@foreach ($errors->all() as $error)
						    	    <li>{{ $error }}</li>
						    	@endforeach
						    </ul></div>
						@endif

						<div class="form-group">
							<label for="name" class="col-md-3 control-label">Name of Teacher</label>
							<div class="col-md-6">
								<input id="name" type="text" class="form-control" name="name" value="{{ $teacher->name }}" autofocus>
							</div>
						</div>

						<div class="form-group">
							<label for="class" class="col-md-3 control-label">Class</label>
							<div class="col-md-6">
								<select id="class" class="form-control" name="class_id">
									<option value=""> -- Choose Class -- </option>
									@foreach($classroom as $classroom)
									    <option value="{{ $classroom->class_id }}" {{ $classroom->class_id == $teacher->class_id ? 'selected' : '' }}>{{ $classroom->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-3 col-md-offset-3">
								<button type="submit" class="btn btn-primary">Save</button>
								<a href="{{ url('teacher') }}" class="btn btn-warning">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection