@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>List of Teacher
						<a href="{{ route('teacher.create') }}" class="btn btn-success pull-right">Insert</a><br></h4>
				</div>

				<div class="panel-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>ID</th>
								<th>Teacher name</th>
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
							@foreach ($teacher as $teacher)
							<tr>
								<td>{{ $teacher->teacher_id }}</td>
								<td>{{ $teacher->name }}</td>
								<td>
									<form method="POST" action="{{ route('teacher.destroy', $teacher->teacher_id) }}">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<a href="{{ route('teacher.edit', $teacher->teacher_id) }}" class="btn btn-primary">Edit</a>
										<button type="submit" class="btn btn-danger">Delete</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection