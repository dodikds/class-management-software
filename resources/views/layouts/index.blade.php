<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}">

    <script type="text/javascript" src="{{ asset('public/js/app.js') }}"></script>
	<title></title>
</head>
<body>

    @yield('content')

</body>
</html>
