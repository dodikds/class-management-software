@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Edit Student<br></h4>
				</div>

				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('student.update', $student->student_id) }}">
						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						@if ($errors->any())
						    <div class="alert alert-danger"><ul>
						    	@foreach ($errors->all() as $error)
						    	    <li>{{ $error }}</li>
						    	@endforeach
						    </ul></div>
						@endif

						<div class="form-group">
							<label for="name" class="col-md-3 control-label">Name of Student</label>
							<div class="col-md-6">
								<input id="name" type="text" class="form-control" name="name" value="{{ $student->name }}" autofocus>
							</div>
						</div>

						<div class="form-group">
							<label for="class" class="col-md-3 control-label">Classroom</label>
							<div class="col-md-6">
								<select id="class" class="form-control" name="class">
									<option value=""> -- Choose Classroom -- </option>
									@foreach($classroom as $classroom)
									    <option value="{{ $classroom->class_id }}" {{ $classroom->class_id == $student->class_id ? 'selected' : '' }}>{{ $classroom->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-3 col-md-offset-3">
								<button type="submit" class="btn btn-primary">Save</button>
								<a href="{{ url('student') }}" class="btn btn-warning">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection