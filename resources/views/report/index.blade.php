@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="konten">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Report List of Classroom</h4>
					</div>

					<div class="panel-body">
						<form action="{{ route('makePDF') }}" method="POST">
							{{ csrf_field() }}
							
							@if ($errors->any())
							    <div class="alert alert-danger"><ul>
							    	@foreach ($errors->all() as $error)
							    	    <li>{{ $error }}</li>
							    	@endforeach
							    </ul></div>
							@endif


						<select id="class_id" class="form-control" name="class_id" width="150">
							<option value=""> -- Choose Classroom -- </option>
	<!-- 						@isset($classroom) -->
							@foreach($classroom as $classroom)
							    <option value="{{ $classroom->class_id }}" 
							    	@isset($class_id)
							    	{{ $classroom->class_id == $class_id ? 'selected' : '' }}
							    	@endisset
							    	>{{ $classroom->name }}</option>
							@endforeach
							<!-- @endisset -->
						</select>
						<br>
						<button type="submit" class="btn btn-primary">Download PDF</button>
						</form>
					</div>

					<div>
						@isset($teacher)
						<b><br>Teacher Name : 
						@foreach ($teacher as $teacher)
						{{ $teacher->name }}</b>
						@endforeach
						@endisset
					</div>

					@isset($student)
					<div>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Student Name</th>
								</tr>
							</thead>

							<tbody>
								@php
									$number = 0
								@endphp
								@foreach ($student as $student)
								<tr>
									@php
										$number++
									@endphp
									<td>{{ $number }}</td>
									<td>{{ $student->name }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@endisset
				</div>
			</div>

			<!-- <a href="{{ route('makePDF') }}" class="btn btn-primary">Download PDF</a> -->

		</div>
	</div>
</div>
@endsection