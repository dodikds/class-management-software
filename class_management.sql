-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2019 at 03:15 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `class_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `class_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_id`, `name`, `teacher_id`, `student_id`, `created_at`, `updated_at`) VALUES
(3, 'Biology', 0, 0, NULL, '2019-05-27 05:28:02'),
(5, 'IT Network', 6, 0, '2019-05-18 21:03:10', '2019-05-19 06:16:06'),
(6, 'Children', 1, 0, '2019-05-18 22:41:10', '2019-05-18 22:41:46'),
(8, 'Cooking', 8, 0, '2019-05-19 02:08:07', '2019-05-19 02:08:07'),
(9, 'Swim', 10, 0, '2019-05-19 02:16:52', '2019-05-19 02:16:52'),
(10, 'Fly On The Sky', 11, 0, '2019-05-19 02:18:01', '2019-05-19 02:18:36'),
(17, 'Indonesian', 1, 0, '2019-05-19 05:15:39', '2019-05-19 05:15:39'),
(20, 'English', 0, 0, '2019-05-19 06:58:17', '2019-05-19 06:58:17'),
(21, 'Sing', 0, 0, '2019-05-19 22:23:20', '2019-05-19 22:23:20'),
(22, 'IT', 0, 0, '2019-05-19 22:24:22', '2019-05-19 22:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_17_210706_create_teacher_table', 1),
(4, '2019_05_17_210743_create_student_table', 1),
(5, '2019_05_18_025518_create_classroom_table', 1),
(6, '2019_05_18_053222_create_class_table', 2),
(7, '2019_05_19_094951_add_column_level_users_table', 3),
(8, '2019_05_19_113114_add_unique_class_table', 4),
(9, '2019_05_19_122420_add_unique_column_name_student_table', 5),
(10, '2019_05_19_123719_add_unique_column_name_teacher_table', 6),
(11, '2019_05_19_140320_change_nullable_column_classid_teacher_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('dodikds@gmail.com', '$2y$10$9XQ0k3gOtFbt4lODO0FjauJu/hKkeqM8aa.LmhQeyAPZKuGtOTaQu', '2019-05-19 00:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `name`, `class_id`, `created_at`, `updated_at`) VALUES
(4, 'Eni Sagita', 21, NULL, '2019-05-21 02:00:54'),
(6, 'Lina Hong', 21, '2019-05-18 23:59:53', '2019-05-21 02:01:03'),
(7, 'Adhyaksa', 22, '2019-05-19 00:00:46', '2019-05-21 02:01:11'),
(9, 'Toni Saputra', 22, '2019-05-19 02:19:51', '2019-05-21 02:01:20'),
(11, 'Nanang', 22, '2019-05-19 05:31:57', '2019-05-21 02:01:29'),
(13, 'Sagita', 21, '2019-05-19 08:40:00', '2019-05-19 22:38:18'),
(14, 'Claudia', 5, '2019-05-21 02:02:34', '2019-05-21 02:03:07'),
(15, 'Nadia', 5, '2019-05-21 02:02:39', '2019-05-21 02:02:59');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`teacher_id`, `name`, `class_id`, `created_at`, `updated_at`) VALUES
(2, 'Bambang Sukoco', 22, NULL, '2019-05-21 02:01:57'),
(8, 'Nana Bana', 6, '2019-05-19 02:02:30', '2019-05-19 22:17:40'),
(11, 'Bird', 10, '2019-05-19 02:17:39', '2019-05-19 22:25:25'),
(13, 'Anto', 0, '2019-05-19 05:41:29', '2019-05-19 05:41:29'),
(14, 'Paul', NULL, '2019-05-19 07:30:35', '2019-05-19 07:30:35'),
(16, 'Linda', NULL, '2019-05-19 07:31:28', '2019-05-19 08:20:36'),
(17, 'Edward', 18, '2019-05-19 07:49:02', '2019-05-19 22:07:33'),
(20, 'Tomson', 5, '2019-05-19 07:57:58', '2019-05-19 22:19:25'),
(24, 'Herlina', 17, '2019-05-19 21:30:06', '2019-05-19 22:24:49'),
(25, 'Nella', 21, '2019-05-19 22:21:32', '2019-05-19 22:24:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `level`) VALUES
(38, 'Administrator', 'dodikds@gmail.com', '$2y$10$gIU4y1m1LSCRoeFylgVmnuGzLKXMicJN/z7OMqm0.nIPuL7V3Ptne', 'uwGteVQmp3KvdRaqgcjL0sflkgd7WhmQWwxsmGSlntGLbWXe3rwbK7IOdCPK', '2019-05-19 00:07:23', '2019-05-19 00:07:23', 1),
(39, 'Admin', 'admin@gmail.com', '$2y$10$9Xh77g6tEvfxZOkOIdiw4OJ5V38D20fUOT9LBe3.mhEZM7lxTQK6O', 'auq8DqqG3KGy9E7mm27UqiLBJARmK2cVMbgYRBRuwRYoIFOi8hSdLtuMUJxA', '2019-05-19 00:12:43', '2019-05-19 00:12:43', 0),
(40, 'Admin 2', 'admin2@gmail.com', '$2y$10$zFtVwGSAhNX67stOO/g87.MdF6Tk8/p2ncD721wBXOy9HKpIr/Vli', 'sbb62i6kmckqt0E5JhCAQ62VoZmhA57FaBRsJMDrKYmavW2Q3Ape3GkyGK6S', '2019-05-27 05:50:27', '2019-05-27 05:50:27', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`class_id`),
  ADD UNIQUE KEY `class_name_unique` (`name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_id`),
  ADD UNIQUE KEY `student_name_unique` (`name`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`teacher_id`),
  ADD UNIQUE KEY `teacher_name_unique` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `teacher_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
