<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['middleware' => ['web', 'checkuserlevel:1']], function() {
Route::group(['middleware' => 'auth'], function() {
    Route::resource('classroom', 'ClassroomController');
	Route::resource('teacher', 'TeacherController');
	Route::resource('student', 'StudentController');
	Route::post('reportshow', 'ReportController@showlist')->name('reportshow');
	Route::resource('report', 'ReportController');
	Route::post('makePDF', 'ReportController@makePDF')->name('makePDF');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');