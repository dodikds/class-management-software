<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $table = 'class';
    protected $primaryKey = 'class_id';

    public function teacher()
    {
    	return $this->belongsTo('App\Teacher', 'foreign_key');
    }

    public function student()
    {
    	return $this->hasMany('App\Student', 'student_id');
    }
}
