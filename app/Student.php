<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    protected $primaryKey = 'student_id';

    public function class()
    {
    	return $this->belongsTo('App\Class', 'class_id');
    }
}
