<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
	protected $table = 'teacher';
	protected $primaryKey = 'teacher_id';

    public function class()
    {
    	return $this->hasMany('App\Class', 'class_id');
    }
}
