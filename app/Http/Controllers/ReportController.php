<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Classroom;
use App\Teacher;
use App\Student;
use Redirect;
use PDF;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $classroom = Classroom::all();
        // $teacher = Teacher::all();
        // $student = Student::all();


        // $classroom = DB::table('class')
        //     ->join('teacher', 'class.class_id', '=', 'teacher.class_id')
        //     ->select('class.*', 'teacher.name')
        //     ->get();

        return view('report.index', compact('classroom', 'teacher', 'student'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
                
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showlist(Request $request)
    { 
        $class_id = $request['class_id'];
        $teacher = DB::select('select * from teacher where class_id = :class_id', ['class_id' => $request['class_id']]);
        $student = DB::select('select * from student where class_id = :class_id', ['class_id' => $request['class_id']]);
        $classroom = Classroom::all();
        // $results = DB::select('select * from teacher where class_id = 5');
        return view('report.index', compact('teacher', 'student', 'classroom', 'class_id'));
    }

    public function makePDF(Request $request)
    { 
        $class_id = $request['class_id'];
        $teacher = DB::select('select * from teacher where class_id = :class_id', ['class_id' => $request['class_id']]);
        $student = DB::select('select * from student where class_id = :class_id', ['class_id' => $request['class_id']]);
        $classroom = Classroom::all();
        
        $pdf = PDF::loadView('report.index', compact('teacher', 'student', 'classroom', 'class_id'));
        return $pdf->download('listofclass.pdf');
    }
}
