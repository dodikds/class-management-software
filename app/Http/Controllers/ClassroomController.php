<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Classroom;
use App\Teacher;
use App\Student;
use Redirect;
use PDF;

class ClassroomController extends Controller
{
    protected $message = array(
        'name.required' => 'Fill name of Classroom',
        'teacher.required' => 'Please choose a teacher',

    );

    protected $rules = array(
        'name' => 'required|unique:class',
        // 'teacher' => 'required',

    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classroom = Classroom::all();
        $teacher = Teacher::all();

        return view('classroom.index', compact('classroom', 'teacher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = Teacher::all();
        $student = Student::all();

        return view('classroom.create', compact('teacher', 'student'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules, $this->message);

        $classroom = new Classroom;
        $classroom->name = $request['name'];
        // $classroom->teacher_id = $request['teacher'];
        $classroom->save();

        return Redirect::route('classroom.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::all();
        $student = Student::all();
        $classroom = Classroom::find($id);

        return view('classroom.edit', compact('classroom', 'teacher', 'student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules, $this->message);

        $classroom = Classroom::find($id);
        $classroom->name = $request['name'];
        $classroom->teacher_id = $request['teacher'];
        $classroom->update();

        return Redirect::route('classroom.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classroom = Classroom::find($id);
        $classroom->delete();

        return Redirect::route('classroom.index');
    }

    public function makePDF(Request $request)
    {
        $teacher = DB::select('select * from teacher where class_id = :class_id', ['class_id' => $request['classroom']]);
        $student = DB::select('select * from student where class_id = :class_id', ['class_id' => $request['classroom']]);
        $class = Classroom::all();

        $pdf = PDF::loadView('report.index', compact('teacher', 'student', 'class'));
        $pdf->setPaper('a4', 'potrait');

        return $pdf->stream();
    }
}
