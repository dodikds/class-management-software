<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Classroom;
use Redirect;
use Closure;

class TeacherController extends Controller
{
    protected $message = array(
        'name.required' => 'Fill name of Teacher',
    );

    protected $rules = array(
        'name' => 'required|unique:teacher',
        'class_id' => 'unique:class',
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher = Teacher::all();
        $classroom = Classroom::all();

        return view('teacher.index', compact('teacher', 'classroom'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = Teacher::all();
        $classroom = Classroom::all();

        return view('teacher.create', compact('teacher', 'classroom'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules, $this->message);

        $teacher = new Teacher;
        $teacher->name = $request['name'];
        $teacher->class_id = $request['classroom'];
        $teacher->save();

        return Redirect::route('teacher.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);
        $classroom = Classroom::all();

        return view('teacher.edit', compact('teacher', 'classroom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, $this->rules, $this->message);

        $teacher = Teacher::find($id);
        $teacher->name = $request['name'];
        $teacher->class_id = $request['class_id'];
        $teacher->update();

        return Redirect::route('teacher.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::find($id);
        $teacher->delete();

        return Redirect::route('teacher.index');
    }
}
